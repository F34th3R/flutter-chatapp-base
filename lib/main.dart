import 'package:flutter/material.dart';
import 'package:flutter_material_chat/views/home_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            debugShowCheckedModeBanner: false,
            title: "Chat App",
            theme: new ThemeData(
                brightness: Brightness.dark,
                primaryColor: Colors.blueGrey[900],
                accentColor: Colors.cyan[600],
            ),
            home: new HomePage(),
        );
    }
}
import 'package:flutter/material.dart';
import 'package:flutter_material_chat/views/chat_screen.dart';

class HomePage extends StatelessWidget {
    @override
      Widget build(BuildContext context) {
        return new Scaffold(
            appBar: new AppBar(
                title: new Text("F34th3R Chat"),
            ),
            body: new ChatScreen(),
        );
      }
}